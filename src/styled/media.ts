export const MEDIA_QUERY_LG = '@media (min-width: 1140px)';
export const MEDIA_QUERY_MD = '@media (min-width: 992px)';
export const MEDIA_QUERY_SM = '@media (min-width: 768px)';
export const MEDIA_QUERY_XS = '@media (min-width: 577px)';