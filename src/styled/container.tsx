import styled from 'styled-components';

import {
  MEDIA_QUERY_LG,
  MEDIA_QUERY_MD,
  MEDIA_QUERY_SM,
  MEDIA_QUERY_XS,
} from './media';

export const Container = styled.div`
  margin-right: auto;
  margin-left: auto;
  width: 100%;
  padding-left: 15px;
  padding-right: 15px;

  ${MEDIA_QUERY_XS} {
    max-width: 540px;
    padding-left: 0;
    padding-right: 0;
  }

  ${MEDIA_QUERY_SM} {
    max-width: 720px;
  }

  ${MEDIA_QUERY_MD} {
    max-width: 960px;
  }

  ${MEDIA_QUERY_LG} {
    max-width: 1010px;
  }
`;