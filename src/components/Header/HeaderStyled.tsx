import React from "react";
import styled from 'styled-components';

export const Head = styled.header`
  padding-top: 10px;
  padding-bottom: 10px;
  background-color: #019fe4;
`;

export const Logo = styled(
  ({src, ...props}) => <img src={src} {...props} />
)`
  width: 40px;
`;

export const Body = styled.div`
  display: flex;
`;

export const User = styled.div`
   width: 40px;
   height: 40px;
   background-color: red;
   border-radius: 20px;
`;

export const Fill = styled.div`
  margin-left: auto;
`;
