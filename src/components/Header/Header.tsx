import React from 'react';

import {Head, Logo, Body, User, Fill} from './HeaderStyled';
import {Container} from "../../styled/container";


import iconLogo from './img/logo.png';

const Header: React.FC = () => {
  return (
    <Head>
      <Container>
        <Body>
          <Logo src={iconLogo} />
          <Fill />
          <User>

          </User>
        </Body>
      </Container>
    </Head>
  )
};

export default Header;